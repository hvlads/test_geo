import csv
import xlrd
from itertools import combinations
from geopy.distance import great_circle

path = 'cities.xlsx'
workbook = xlrd.open_workbook(path)
worksheet = workbook.sheet_by_index(0)

offset = 1

if __name__ == '__main__':
    rows = []
    for i, row in enumerate(range(worksheet.nrows)):
        if i <= offset:
            continue
        r = []
        for j, col in enumerate(range(worksheet.ncols)):
            r.append(worksheet.cell_value(i, j))
        rows.append(r)
    m = []
    for x in combinations([tuple(y[3:]) for y in rows], 2):
        print(x)
        try:
            m.append([x[0][0], x[0][1], x[1][0], x[1][1], great_circle(x[0], x[1]).miles])
        except ValueError as ex:
            continue

    with open("out.csv", "w+") as out_csv:
        csvWriter = csv.writer(out_csv, delimiter=',')
        csvWriter.writerows(m)
